<h1 style="text-align: center;"> 💜 wxz-admin 💜</h1>

<p style="text-align: center;">
<a href="https://nodejs.org/en/about/releases/"><img src="https://img.shields.io/node/v/vite.svg" alt="node"></a>
  <a href="https://github.com/vuejs/core">
    <img src="https://img.shields.io/badge/vue-3.2.13-brightgreen.svg" alt="vue">
  </a>
  <a href="https://gitee.com/wang-xiaoze"><img alt="author" src="https://img.shields.io/badge/author-WangXiaoZe-blue.svg"/></a>
  <a href="https://gitee.com/wang-xiaoze/simplify-admin/blob/master/LICENSE"><img alt="LICENSE" src="https://img.shields.io/github/license/ElanYoung/spring-boot-learning-examples.svg"/></a>
</p>

<p style="text-align: center;">
  <b>Vue3</b> + <b>JavaScript/TypeScript</b> + <b>Vue Router</b> + <b>Pinia</b> + <b>Ele-Plus</b> + <b>scss</b> + <b>Axios</b> + <b>ESLint + Stylelint + Prettier</b>
</p>

## 介绍

在`simple-admin`的基础上进行升级改版，原先`simple-admin`将会停止更新，原先中后台仅仅是为了对主题进行练手而搭建；
现在将进行整个中后台系统的开发；

最新版本将会使用`vite + ts + vue3 + pinia`搭建完成一个简单上手的中后台系统；
`wxz-admin`是一款基于`element-plus`免费开源的中后台模板，继承最先进的前端技术，从而达到简单模板
的开发即用的前端中后台方案，当然也可以当成一个练手的开源项目；

## 特性
> 暂定为以下几类：
- [ ] **技术栈要求**：`vue3 + vite + pinia + Es6+`
- [ ] **主题**：可配置化主题开发，设计布局，颜色，其他配置项等；
- [ ] **权限**：路由权限，页面权限，按钮权限等；
- [ ] **国际化**：内置完善的国际化方案，目前暂定义为`中英`俩套语言；
- [ ] **可视化数据**：`Echarts`可视化数据解决方案；
- [ ] **自定义数据**：内置`Mock`数据方案；
- [ ] **封装类型hooks**：实现`hooks`的重要性；
- [ ] **代码规范，提交规范**：规范代码书写格式以及提交规范；
- [ ] **其他**：内嵌部分个人项目

## 预览
- 暂未开发完成
- 账号密码：默认账号 无需修改

## 项目准备
- `Node， git`， `git` 提交规范
- `vue3, Es6` 特性以及语法
- `Element-plus` 基础使用
- `pinia` 熟悉使用
- `Mock.js` 基本语法

## 安装使用
- 拉取源码

```shell
git clont https://github.com/wangxiaoze-view/wxz-admin.git
```

- 安装依赖

```shell
pnpm install
// 如果pnpm install安装不了的情况, 可以使用cnpm
cnpm install
```
- 项目运行
```shell
pnpm run dev
```

- 打包
```shell
  pnpm run build
```
